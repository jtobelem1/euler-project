# euler-project

![euler level](https://projecteuler.net/profile/josselin.tobelem.png)

Ce projet contient des notebooks pour répondre aux problèmes de :

https://projecteuler.net/archives

La solution des 50 premiers problèmes sont publiés sur ce repo en accord avec https://projecteuler.net/about.

La plupart des solutions sont implémentées avec python/pandas. Pour certains problèmes, j'ai utilisé le moteur de calcul sage-math.

Le dossier stat-euler est un projet pour scrapper des données sur les utilisateurs et les problèmes du projet afin de les visualiser.

## Exectuer ce repo sur mybinder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jtobelem1%2Feuler-project/HEAD?urlpath=lab?filepath=1-50.ipynb)
